import numpy as np
from bokeh.layouts import column
from bokeh.plotting import figure, show, curdoc
from bokeh.models import Button, Slider
import functools

# make unit circle
circrange = np.linspace(0, 2*np.pi, 100)[:-1]
cxs = np.cos(circrange)
cys = np.sin(circrange)

def pol3(coeffs):
    """ call 3 polynomials """
    f = functools.partial(np.polynomial.polynomial.polyval2d, cxs, cys)
    return np.stack([f(coeffs[0]), f(coeffs[1]), f(coeffs[2])])


def color_str(rgb):
    assert rgb.shape == (3,), rgb.shape
    r, g, b = rgb.astype(np.uint8)
    return f"#{r:02x}{g:02x}{b:02x}"


plot = figure()
color_renderer = None

def random_colorloop():
    coeffs = np.random.rand(3,3,3)
    upperleft = np.triu(
        np.ones((3,3,3), dtype=bool)
    )[:,:,::-1]
    coeffs *= upperleft
    coeffs = (coeffs - 0.5) * 2

    # map circle into 3d color loop with polyn. coeffs
    colorloop = pol3(coeffs)

    # normalize
    colorloop = (colorloop - np.min(colorloop)) / (np.max(colorloop) - np.min(colorloop))
    colorloop *= 255

    # calculate hex color strings
    colors = np.apply_along_axis(color_str, 0, colorloop)

    # do circles
    global color_renderer
    # if not color_renderer:
    color_renderer = plot.circle(
        cxs,
        cys,
        fill_color=colors,
        radius=0.07,
        line_alpha=0,
    )


# call random_colorloop whenever button is pressed
button = Button(label="Press Me")
button.on_click(random_colorloop)

# display button and plot over webserver
curdoc().add_root(column(button, plot))
